/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.shapeoop;

/**
 *
 * @author ASUS
 */
public class Square extends Shape{
    protected double side;
    public Square (double side) {
        super("Rectangle");
        this.side = side;
    }
    public void print(){
        System.out.println("Area of " + getName()+" is "+ calArea());
    }
}
