/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.shapeoop;

/**
 *
 * @author ASUS
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;

    public Rectangle(double width ,double height) {
        super("Rectangle");
        this.width = width;
        this.height = height;
    }
    public double calArea() {
        return  width*height;
    }
    public void print(){
        System.out.println("Area of " + getName()+" is "+ calArea());
    }
}


