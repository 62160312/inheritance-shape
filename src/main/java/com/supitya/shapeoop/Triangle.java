/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supitya.shapeoop;

/**
 *
 * @author ASUS
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base ,double height){
        super("Triangle");
        this.base = base;
        this.height = height;
    }
    @Override
    public double calArea() {
        return  0.5*base*height;
    }
    public void print(){
        System.out.println("Area of " + getName()+" is "+ calArea());
    }
}
